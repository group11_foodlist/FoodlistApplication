//
//  Recipe.swift
//  FoodListApp
//
//  Created by Dennis Jutemark on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import Foundation

class Recipe: Codable {
    let servings, id, readyInMinutes: Int?
    var week, day: Int?
    
    let title, image, imageType, instructions: String?
    
    var extendedIngredients:[Ingredient]?
    
    init(recipe: DataRecipe) {
        self.id = Int(recipe.id)
        self.image = recipe.image
        self.imageType = recipe.imageType
        self.instructions = recipe.instructions
        self.readyInMinutes = Int(recipe.readyInMinutes)
        self.servings = Int(recipe.servings)
        self.title = recipe.title
        
        self.week = Int(recipe.week)
        self.day = Int(recipe.day)
        
        self.extendedIngredients = [Ingredient]()
    }
    init() {
        self.id = -1
        self.image = ""
        self.imageType = ""
        self.instructions = ""
        self.readyInMinutes = -1
        self.servings = -1
        self.title = ""
        
        self.week = -1
        self.day = -1
        
        self.extendedIngredients = nil
    }
}

class RandomRecipeResponse: Codable {
    let recipes: [Recipe]?
}

class SearchRecipeResponse: Codable {
    let results: [Recipe]?
    let baseUri: String?
}
