//
//  Ingredient.swift
//  FoodListApp
//
//  Created by Dennis Jutemark on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import Foundation

class Ingredient: Codable {
    let id: Int?
    var week, recipeID: Int?
    var amount: Double?
    let image, name, unit, unitShort, unitLong, originalString: String?
    let metaInformation: [String]?
    
    init(id: Int?, recipeID: Int?, amount: Double?, aisle: String?, image: String?, name: String?, unit: String?, unitShort: String?, unitLong: String?, originalString: String?, metaInformation: [String]?, week: Int?) {
        
        self.id = id
        self.recipeID = recipeID
        self.amount = amount
        self.image = image
        self.name = name
        self.unit = unit
        self.unitShort = unitShort
        self.unitLong = unitLong
        self.originalString = originalString
        self.metaInformation = metaInformation
        self.week = week
    }
    
    init(ingredient: DataIngredient) {
        self.id = Int(ingredient.id)
        self.recipeID = Int(ingredient.recipeID)
        self.amount = ingredient.amount?.doubleValue
        self.image = ingredient.image
        self.name = ingredient.name
        self.unit = ingredient.unit
        self.unitShort = ingredient.unitShort
        self.unitLong = ingredient.unitLong
        self.week = Int(ingredient.week)
        
        self.originalString = nil
        self.metaInformation = nil
    }
}
