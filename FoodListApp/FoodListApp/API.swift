//
//  API.swift
//  FoodListApp
//
//  Created by Dennis Jutemark on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import Foundation

private func getHasDiet() -> String {
    let prefs = UserDefaults.standard
    let diet = prefs.string(forKey: "PICKED_DIET")
    if diet == nil { return ""}
    return diet!
}

class API {
    
    static let scheme = "https"
    static let host = "spoonacular-recipe-food-nutrition-v1.p.mashape.com"
    static let secret = "efrv3XUdw8msh9IysiOIjg8IjMEmp1dbCLHjsn9DzgoWPPisVy"
    
    public static func CreateCommaSeperatedStringFromList(list: [String]) -> String {
        var string = ""
        
        for element in list {
            string += element + ","
        }
        string.removeLast()
        return string
    }
    
    public class Get {
        public static func createURLRequest(path: String, query: String = "") -> URLRequest {
            var urlComponents = URLComponents()
            urlComponents.scheme = API.scheme
            urlComponents.host = API.host
            urlComponents.path = path
            
            if query.count > 0 {
                urlComponents.query = query
            }
            
            var request = URLRequest(url: urlComponents.url!)
            request.httpMethod = "GET"
            request.addValue(API.secret, forHTTPHeaderField: "X-Mashape-Key")
            
            return request
        }
        
        public class ValidatedGetResponse {
            public var valid: Bool
            public var json: Data?
            
            init(valid: Bool, json: Data?) {
                self.valid = valid
                self.json = json
            }
        }
        
        public static func validateGetResponse(data: Data?, error: Error?) -> ValidatedGetResponse {
            guard error == nil else {
                return ValidatedGetResponse(valid: false, json: nil)
            }
            
            guard let jsonData = data else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                print("Could not set jsonData to data, error =", error)
                return ValidatedGetResponse(valid: false, json: nil)
            }
            
            return ValidatedGetResponse(valid: true, json: jsonData)
        }
    
        
        public class Recipes {
            public static func Random(amount: Int = 1, tags: [String] = [String](), completion: ((_ recipes: [Recipe]?) -> Void)?) {
                // /recipes/random/number=5&diet=Vegan
                URLSession(configuration: URLSessionConfiguration.default).dataTask(with: API.Get.createURLRequest(path: "/recipes/random", query: "number=\(amount)&tags=\(getHasDiet()),dinner" + (tags.count > 0 ? API.CreateCommaSeperatedStringFromList(list: tags) : "" ))) { (data, response, error) in
                    DispatchQueue.main.async {
                        print(response?.url?.absoluteString as Any)
                        let valid = API.Get.validateGetResponse(data: data, error: error)
                        
                        if valid.valid == false || valid.json == nil {
                            completion?(nil)
                            return
                        }
                        
                        do {
                            let recipesData = try JSONDecoder().decode(RandomRecipeResponse.self, from: valid.json!)
                            completion?(recipesData.recipes)
                        } catch {
                            completion?(nil)
                        }
                    }
                }.resume()
            }
            
            public static func Information(id: Int, completion: ((_ recipes: Recipe?) -> Void)?) {
                URLSession(configuration: URLSessionConfiguration.default).dataTask(with: API.Get.createURLRequest(path: "/recipes/\(id)/information", query: "includeNutrition=false")) { (data, response, error) in
                    let valid = API.Get.validateGetResponse(data: data, error: error)
                    
                    if valid.valid == false || valid.json == nil {
                        completion?(nil)
                        return
                    }
                    
                    do {
                        let recipe = try JSONDecoder().decode(Recipe.self, from: valid.json!)
                        completion?(recipe)
                    } catch {
                        completion?(nil)
                    }
                }.resume()
            }
            
            public static func Search(cuisines: [String] = [String](), diet: String = "", excludeIngredients: [String] = [String](), instructionsRequired: Bool = true, intolerances: [String] = [String](), limitLicense: Bool = false, number: Int = 10, offset: Int = 0, query: String, type: String, completion: ((_ recipes: [Recipe]?, _ baseUri: String?) -> Void)?) {
                var searchQuery = ""
                
                if cuisines.count > 0 { searchQuery += "cuisines=" + API.CreateCommaSeperatedStringFromList(list: cuisines) + "&" }
                if diet.count > 0 { searchQuery += "diet=\(diet)&" }
                if excludeIngredients.count > 0 { searchQuery += "excludeIngredients=" + API.CreateCommaSeperatedStringFromList(list: excludeIngredients) + "&" }
                searchQuery += "instructionsRequired=" + (instructionsRequired ? "true" : "false") + "&"
                if intolerances.count > 0 { searchQuery += "intolerances=" + API.CreateCommaSeperatedStringFromList(list: intolerances) + "&" }
                searchQuery += "limitLicense=" + (limitLicense ? "true" : "false") + "&"
                searchQuery += "number=\(number)&"
                searchQuery += "offset=\(offset)&"
                searchQuery += "query=\(query)&"
                searchQuery += "type=\(type)"
                
                URLSession(configuration: URLSessionConfiguration.default).dataTask(with: API.Get.createURLRequest(path: "/recipes/search", query: searchQuery)) { (data, response, error) in
                    let valid = API.Get.validateGetResponse(data: data, error: error)
                    
                    if valid.valid == false || valid.json == nil {
                        completion?(nil, nil)
                        return
                    }
                    
                    do {
                        let recipes = try JSONDecoder().decode(SearchRecipeResponse.self, from: valid.json!)
                        completion?(recipes.results, recipes.baseUri)
                    } catch {
                        completion?(nil, nil)
                    }
                }.resume()
            }
            
            public static func AutocompleteSearch(query: String, amount: Int = 8, completion: ((_ recipes: [Recipe]?) -> Void)?) {
                URLSession(configuration: URLSessionConfiguration.default).dataTask(with: API.Get.createURLRequest(path: "/recipes/autocomplete", query: "number=\(amount)&query=\(query)")) { (data, response, error) in
                    let valid = API.Get.validateGetResponse(data: data, error: error)
                    
                    if valid.valid == false || valid.json == nil {
                        completion?(nil)
                        return
                    }
                    
                    do {
                        let recipes = try JSONDecoder().decode([Recipe].self, from: valid.json!)
                        completion?(recipes)
                    } catch {
                        completion?(nil)
                    }
                }.resume()
            }
        }
    }
}
