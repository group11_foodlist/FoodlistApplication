//
//  GroceryListViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class GroceryCell: UITableViewCell {
    @IBOutlet weak var ingredientNameLabel: UILabel!
    @IBOutlet weak var groceryAmountLabel: UILabel!
    @IBOutlet weak var groceryUnitLabel: UILabel!
}

class GroceryListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var groceryList: [Ingredient]? = nil
    var summedGroceryList = [Ingredient]()
    
    @IBOutlet weak var groceryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.groceryTableView.delegate = self
        self.groceryTableView.dataSource = self
        let date = Date()
        let calendar = Calendar.current
        let week = calendar.component(.weekOfYear, from: date)
        groceryList = Storage.Ingredients.Get.Week(week: week)
        
        if groceryList != nil {
            for grocery in groceryList! {
                var indexAtSummed = 0
                var isIngredientFound = false
                
                for summedGrocery in summedGroceryList {
                    indexAtSummed += 1
                    if grocery.id ?? -1 == summedGrocery.id ?? -2 && grocery.unitShort ?? "not" == summedGrocery.unitShort ?? "equal" {
                        isIngredientFound = true
                        break
                    }
                }
                if isIngredientFound && indexAtSummed != summedGroceryList.count && summedGroceryList[indexAtSummed].amount != nil && grocery.amount != nil {
                    summedGroceryList[indexAtSummed].amount! += grocery.amount!
                } else {
                    summedGroceryList.append(grocery)
                }
            }
            for grocery in summedGroceryList {
                print(grocery.name as Any, grocery.amount as Any)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return summedGroceryList.count
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Ingredients"
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groceryCell") as! GroceryCell
        cell.ingredientNameLabel.text = summedGroceryList[indexPath.row].name
        cell.groceryAmountLabel.text = String.init(format: "%.2f", summedGroceryList[indexPath.row].amount ?? "")
        cell.groceryUnitLabel.text = summedGroceryList[indexPath.row].unitLong
        
        return cell
    }
}
