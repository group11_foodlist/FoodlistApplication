//
//  WeekViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class FoodOfDayCell: UITableViewCell {
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodLabel: UILabel!
}

class WeekViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var wofTableView: UITableView!
    let sections = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    var recipesForWeek = [Recipe]()
    let date = Date()
    let calendar = Calendar.current
    var currentWeek: Int = 666
    var currentDay: Int = 666
    var fetchedRecipes = false
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.wofTableView.delegate = self
        self.wofTableView.dataSource = self
        currentWeek = calendar.component(.weekOfYear, from: date)
        currentDay = calendar.component(.weekday, from: date) - 2
        
        if currentDay < 0 { currentDay += 7 }
    }
    override func viewDidAppear(_ animated: Bool) {
        if !fetchedRecipes {
            let tempRecipes = Storage.Recipes.Get.Week(week: self.currentWeek)
            
            if tempRecipes == nil || tempRecipes?.count == 0 {
                generateAlert(completion: { (clickedYes) in
                    if clickedYes {
                        self.generateNewWeek()
                    }
                })
            } else {
                self.fillEmptyRecipesForWeek()
                self.appendRecipesForWeek(recipes: tempRecipes!)
                self.generateRows()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier != "weekToGrocerySegue" {
            let vc = segue.destination as! EditFoodViewController
            if let foodToEdit = (sender as? FoodOfDayCell)?.foodLabel.text {
                for foodLabelToEdit in recipesForWeek {
                    if foodLabelToEdit.title == foodToEdit {
                        vc.food = foodLabelToEdit
                        break
                    }
                }
            }
        }
    }
    private func generateNewWeek() {
        let amountOfRecipesToGenerate = 7 - currentDay
        
        API.Get.Recipes.Random(amount: amountOfRecipesToGenerate) {(recipes) in
            if recipes == nil {
                print("Could not get ", amountOfRecipesToGenerate, " random recipes...")
                // Todo: Show error message to the user
            } else {
                self.fillEmptyRecipesForWeek()
                self.appendRecipesForWeek(recipes: recipes!)
                self.generateRows()
            }
        }
    }
    
    private func generateRows() {
        recipesForWeek.sort { return $0.day! < $1.day! } // Sort by day - Ascending
        
        var i = currentDay
        
        wofTableView.beginUpdates()
        while i < 7 {
            if wofTableView.cellForRow(at: IndexPath(row: 0, section: i - currentDay)) != nil {
                wofTableView.deleteRows(at: [IndexPath(row: 0, section: i - currentDay)], with: .automatic)
            }
            wofTableView.insertRows(at: [IndexPath(row: 0, section: i - currentDay)], with: .automatic)
            i += 1
        }
        self.fetchedRecipes = true
        wofTableView.endUpdates()
        
        if !saveCurrentWeek() {
            // Show message to user that there were an error while saving the current week
            print("Could not save current week...")
        }
        
        i = currentDay
        while i < 7 {
            URLSession.shared.dataTask(with: NSURL(string: recipesForWeek[i].image!)! as URL, completionHandler:  { (data, response, error) in
                guard error == nil else {
                    return
                }
                DispatchQueue.main.sync(execute: { () -> Void in
                    var index = 0
                    for r in self.recipesForWeek {
                        if r.image == response?.url?.absoluteString {
                            break
                        }
                        index += 1
                    }
                    guard let cell = self.wofTableView.cellForRow(at: IndexPath(row: 0, section: index - self.currentDay)) else {
                        return
                    }
                    (cell as! FoodOfDayCell).foodImage.image = UIImage(data: data!)
                })
            }).resume()
            
            i += 1
        }
    }
    
    private func fillEmptyRecipesForWeek() {
        recipesForWeek.removeAll()
        var i = 0
        while i < self.currentDay {
            self.recipesForWeek.append(Recipe())
            i += 1
        }
    }
    private func appendRecipesForWeek(recipes: [Recipe]) {
        var i = self.currentDay
        for r in recipes {
            r.day = i
            r.week = self.currentWeek
            i += 1
            self.recipesForWeek.append(r)
        }
    }
    private func deleteCurrentWeekIngredients() {
        Storage.Ingredients.Delete.Week(week: currentWeek)
    }
    private func saveCurrentWeek() -> Bool {
        deleteCurrentWeekIngredients()
        return Storage.Recipes.Save.Many(recipes: self.recipesForWeek)
    }
    
    private func generateAlert(completion: ((_ clickedYes: Bool) -> Void)? ) {
        let alert = UIAlertController(title: "New Week", message: "Do you want to generate a new week?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Yes"), style: .default, handler: {_ in
            completion?(true)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: "No"), style: .default, handler: {_ in
            completion?(false)
        }))
        self.present(alert, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedRecipes ? 1 : 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 7 - currentDay
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[ currentDay + section ]
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let s = currentDay + indexPath.section
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodOfDayCell") as! FoodOfDayCell
        print(recipesForWeek.count, s)
        cell.foodLabel.text = recipesForWeek[s].title
        
        return cell
    }
    
    @IBAction func generateWeekButton_Click(_ sender: Any) {
        generateAlert { (clickedYes) in
            self.generateNewWeek()
        }
    }
    
    
    
}
