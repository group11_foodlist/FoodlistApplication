//
//  PreferencesViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

var pickerItems: [String] = [String]()
class PreferencesViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var dietPickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dietPickerView.delegate = self
        self.dietPickerView.dataSource = self
        pickerItems = ["All-eater", "vegan", "vegetarian", "primal", "paleo", "ketogenic"]

        let diet = getHasDiet()
        var x = 0
        for i in pickerItems {
            if diet == i {
                dietPickerView.selectRow(x, inComponent: 0, animated: false)
            }
            x += 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setHasDiet(hasDiet: String) {
        let prefs = UserDefaults.standard
        var diet = hasDiet
        
        if hasDiet == "All-eater" {diet = ""}
        prefs.set(diet, forKey: "PICKED_DIET")
    }
    private func getHasDiet() -> String {
        let prefs = UserDefaults.standard
        if prefs.string(forKey: "PICKED_DIET") == nil {return ""}
        return prefs.string(forKey: "PICKED_DIET")!
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerItems.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("get pickled, son")
        return pickerItems[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("save ", pickerItems[row], " as preffered diet...")
        setHasDiet(hasDiet: pickerItems[row])
    }

}
