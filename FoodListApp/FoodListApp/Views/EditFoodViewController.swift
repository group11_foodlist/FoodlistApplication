//
//  EditFoodViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class IngredientCell: UITableViewCell {
    @IBOutlet weak var tableIngredientName: UILabel!
    @IBOutlet weak var tableAmountLabel: UILabel!
    @IBOutlet weak var tableUnitLabel: UILabel!
}

class EditFoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var food = Recipe()
    var isRandom = false
    var isSearched = false
    
    @IBOutlet weak var recipeDetailTableView: UITableView!
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var foodInstructionsLabel: UITextView!
    @IBOutlet weak var foodImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recipeDetailTableView.delegate = self
        self.recipeDetailTableView.dataSource = self
        print("isRandom = ", isRandom, "isSearched = ", isSearched)
        if isRandom {
            print("going to generate a random recipe")
            API.Get.Recipes.Random() { recipes in
                if recipes != nil && recipes!.count > 0 {
                    self.food = recipes![0]
                    self.populateAsyncRecipe()
                }
            }
            isRandom = false
        } else if isSearched && food.id != nil {
            API.Get.Recipes.Information(id: food.id!) { recipe in
                if recipe != nil {
                    self.food = recipe!
                    self.populateAsyncRecipe()
                }
            }
            
            isSearched = false
        } else {
            populateRecipeDetail()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func populateAsyncRecipe() {
        DispatchQueue.main.async {
            if self.food.extendedIngredients?.count ?? 0 > 0 {
                self.populateIngredients(ingredients: self.food.extendedIngredients!)
            }
            self.populateRecipeDetail()
        }
    }
    
    func populateIngredients(ingredients: [Ingredient]) {
        self.recipeDetailTableView.beginUpdates()
        
        var i = 0
        while i < ingredients.count {
            if self.recipeDetailTableView.cellForRow(at: IndexPath(row: i, section: 0)) != nil {
                self.recipeDetailTableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
            
            self.recipeDetailTableView.insertRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            i += 1
        }
        self.recipeDetailTableView.endUpdates()
        
        self.populateRecipeDetail()
    }
    
    func populateRecipeDetail() {
        foodNameLabel.text = food.title;
        foodInstructionsLabel.text = food.instructions;
        
        URLSession.shared.dataTask(with: NSURL(string: food.image!)! as URL, completionHandler:  { (data, response, error) in
            print(response?.url?.absoluteString as Any)
            guard error == nil else {
                return
            }
            DispatchQueue.main.sync(execute: { () -> Void in
                self.foodImageView.image = UIImage(data:data!)
            })
        }).resume()
        }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return food.extendedIngredients?.count ?? 0
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Ingredients"
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ingredientCell") as! IngredientCell
        if food.extendedIngredients != nil {
            cell.tableIngredientName.text = food.extendedIngredients![indexPath.row].name
            cell.tableUnitLabel.text = food.extendedIngredients![indexPath.row].unitLong
            cell.tableAmountLabel.text = String.init(format: "%.2f", food.extendedIngredients![indexPath.row].amount ?? "")
        }
        return cell
    }
}
