//
//  ViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-10-30.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

var isMenuActive = false
var isSearchBarActive = false

class ViewController: UIViewController, UISearchBarDelegate {
   
    @IBOutlet weak var foodSearchBar: UISearchBar!
    @IBOutlet weak var todaysFoodButton: UIButton!
    @IBOutlet weak var todaysFoodLabel: UILabel!
    @IBOutlet weak var slidingViewMain: UIView!
    @IBOutlet weak var trailingConstraintSlidingView: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintSlidingView: NSLayoutConstraint!
    @IBOutlet weak var currentViewButton: UIButton!
    
    @IBAction func hamburgerMenu_Button(_ sender: Any) {
        if !isMenuActive {
            trailingConstraintSlidingView.constant = -150
            leadingConstraintSlidingView.constant = 150
            isMenuActive = true
        } else {
            trailingConstraintSlidingView.constant = 0
            leadingConstraintSlidingView.constant = 0
            isMenuActive = false
        }
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) {( animationComplete) in print("Animation Complete...")}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        currentViewButton.isEnabled = false
        currentViewButton.setTitleColor(UIColor.darkGray, for: UIControlState.disabled)
        self.foodSearchBar.delegate = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        trailingConstraintSlidingView.constant = 0
        leadingConstraintSlidingView.constant = 0
        isMenuActive = false
        
        let today = getTodaysDay()
        let thisWeek = getTodaysWeek()
        let todaysRecipe = Storage.Recipes.Get.Day(week: thisWeek, day: today)
        
        if todaysRecipe != nil {
            URLSession.shared.dataTask(with: NSURL(string: todaysRecipe!.image!)! as URL, completionHandler:  { (data, response, error) in
                print("error ? ", error as Any)
                print(response?.url?.absoluteString as Any)
                
                guard error == nil else {
                    return
                }
                DispatchQueue.main.sync(execute: { () -> Void in
                    self.todaysFoodButton.setImage(UIImage(data: data!), for: UIControlState.normal)
                })
            }).resume()
            todaysFoodLabel.text = todaysRecipe?.title
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "todaysFoodToDetailsSegue") {
            print("\n\nStarting segue to edit food...")
            let segueDest = segue.destination as! EditFoodViewController
            let today = getTodaysDay()
            let thisWeek = getTodaysWeek()
            let todaysRecipe = Storage.Recipes.Get.Day(week: thisWeek, day: today)
            
            segueDest.food = todaysRecipe!
        }
        if (segue.identifier == "foodSearchSegue") {
            let vc = segue.destination as! FoodSearchViewController
            vc.searchQueryString = foodSearchBar.text!
        }
        if (segue.identifier == "surpriseMeSegue") {
            let segueDest = segue.destination as! EditFoodViewController
            segueDest.isRandom = true
        }
        if (segue.identifier == "foodSearchSegue") {
            let segueDest = segue.destination as! FoodSearchViewController
            segueDest.searchQueryString = foodSearchBar.text!
        }
    }
    
    func getTodaysDay() -> (Int) {
        let date = Date()
        let calendar = Calendar.current
        
        var day = calendar.component(.weekday, from: date) - 2
        if day < 0 { day += 7 }
        return day
    }
    func getTodaysWeek() -> (Int) {
        let date = Date()
        let calendar = Calendar.current
        let week = calendar.component(.weekOfYear, from: date)
        return week
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchBarActive = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        performSegue(withIdentifier: "foodSearchSegue", sender: foodSearchBar.text)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.foodSearchBar.endEditing(true)
        isSearchBarActive = false
    }
}

