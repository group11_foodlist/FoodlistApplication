//
//  FoodSearchViewController.swift
//  FoodListApp
//
//  Created by Fredrik Holm on 2017-11-20.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class SearchResultRow : UITableViewCell {
    @IBOutlet weak var FoodNameLabel: UILabel!
    @IBOutlet weak var FoodImageView: UIImageView!
    
}
class FoodSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var SearchResultTable: UITableView!
    
    var searchQueryString = ""
    var amountOfExpectedResults = 3
    var amountOfResultsFromSearch = 0
    var recipes = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SearchResultTable.delegate = self
        self.SearchResultTable.dataSource = self
        
        API.Get.Recipes.Search(diet: "", number: amountOfExpectedResults, query: searchQueryString, type: "main course") { (recipes, baseUri) in
            if recipes != nil && recipes!.count > 0 {
                DispatchQueue.main.async {
                    self.SearchResultTable.beginUpdates()
                    
                    var i = 0
                    while i < self.amountOfResultsFromSearch {
                        if self.SearchResultTable.cellForRow(at: IndexPath(row: i, section: 0)) != nil {
                            self.SearchResultTable.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
                        }
                        i += 1
                    }
                    
                    i = 0
                    for _ in recipes! {
                        self.SearchResultTable.insertRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
                        i += 1
                    }
                    
                    self.recipes = recipes!
                    self.amountOfResultsFromSearch = recipes!.count
                    self.SearchResultTable.endUpdates()
                    
                    i = 0
                    while i < self.amountOfResultsFromSearch {
                        let s = (baseUri ?? "") + (recipes![i].image ?? "")
                        print("Going to search with ", s)
                        
                        URLSession.shared.dataTask(with: NSURL(string: s )! as URL, completionHandler:  { (data, response, error) in
                            print(response?.url?.absoluteString as Any)
                            print(data!)
                            
                            guard error == nil else {
                                return
                            }
                            DispatchQueue.main.sync(execute: { () -> Void in
                                var index = 0
                                for recipe in self.recipes {
                                    if recipe.image != nil && baseUri! + recipe.image! == response?.url?.absoluteString {
                                        break
                                    }
                                    index += 1
                                }
                                
                                let cell = self.SearchResultTable.cellForRow(at: IndexPath(row: index, section: 0)) as? SearchResultRow
                                
                                if cell != nil {
                                    cell!.FoodImageView.image = UIImage(data: data!)
                                }
                            })
                        }).resume()
                        
                        i += 1
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! EditFoodViewController
        
        if let foodToEdit = (sender as? SearchResultRow)?.FoodNameLabel.text {
            for recipe in recipes {
                if recipe.title == foodToEdit {
                    print("Found! Going to set food")
                    print(recipe.title ?? "Generic Title", recipe.extendedIngredients?.count ?? "Generic Ingredient", recipe.image ?? "Generic Image")
                    vc.food = recipe
                    vc.isSearched = true
                    break
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amountOfResultsFromSearch
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "search-results"
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Going to create a new cell!")
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultRow") as! SearchResultRow
        cell.FoodNameLabel.text = recipes[indexPath.row].title
        
        return cell
    }
}
