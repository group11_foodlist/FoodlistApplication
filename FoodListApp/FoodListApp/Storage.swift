//
//  Storage.swift
//  FoodListApp
//
//  Created by Dennis Jutemark on 2017-11-27.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Storage {
    public static func GetManagedContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    class Recipes {
        public static func GetFetchRequest() -> NSFetchRequest<NSManagedObject> {
            return NSFetchRequest<NSManagedObject>(entityName: "DataRecipe")
        }
        
        class Get {
            static func printError(error: NSError) {
                print("Could not get :( \n\(error). \(error.userInfo)")
            }
            
            public static func One(predicate: NSPredicate) -> Recipe? {
                var recipes: [DataRecipe]
                let fetchRequest = Recipes.GetFetchRequest()
                fetchRequest.predicate = predicate
        
                do {
                    recipes = try Storage.GetManagedContext()!.fetch(fetchRequest) as! [DataRecipe]
                    
                    if recipes.count == 0 {
                        return nil
                    }
                    let recipe = Recipe(recipe: recipes[0])
                    
                    recipe.extendedIngredients = Ingredients.Get.RecipeIngredients(recipe: recipe)
                    
                    return recipe
                } catch let error as NSError {
                    Storage.Recipes.Get.printError(error: error)
                    return nil
                }
            }
            
            public static func Many(predicate: NSPredicate) -> [Recipe]? {
                let fetchRequest = Recipes.GetFetchRequest()
                fetchRequest.predicate = predicate
                
                do {
                    let dataRecipes = try Storage.GetManagedContext()!.fetch(fetchRequest) as! [DataRecipe]
                    var recipes = [Recipe]()
                    
                    for recipe in dataRecipes {
                        let r = Recipe(recipe: recipe)
                        r.extendedIngredients = Ingredients.Get.RecipeIngredients(recipe: r)
                        
                        recipes.append(r)
                    }
                    
                    return recipes
                } catch let error as NSError {
                    Storage.Recipes.Get.printError(error: error)
                    return nil
                }
            }
            
            public static func Week(week: Int) -> [Recipe]? {
                return Many(predicate: NSPredicate(format: "week = \(week)"))
            }
            
            public static func Day(week: Int, day: Int) -> Recipe? {
                return One(predicate: NSPredicate(format: "week = %@ AND day = %@", argumentArray: [week, day]))
            }
            
            public static func All() -> [Recipe]? {
                do {
                    let dataRecipes = try Storage.GetManagedContext()!.fetch(Recipes.GetFetchRequest()) as! [DataRecipe]
                    var recipes = [Recipe]()
                    
                    for recipe in dataRecipes {
                        let r = Recipe(recipe: recipe)
                        r.extendedIngredients = Ingredients.Get.RecipeIngredients(recipe: r)
                        recipes.append(r)
                    }
                    
                    return recipes
                } catch let error as NSError {
                    Storage.Recipes.Get.printError(error: error)
                    return nil
                }
            }
        }
        
        class Save {
            static func printError(error: NSError) {
                print("Could not save :( \n\(error). \(error.userInfo)")
            }
            
            public static func createRecipeEntity(storage: NSManagedObjectContext) -> DataRecipe {
                return NSEntityDescription.insertNewObject(forEntityName: "DataRecipe", into: storage) as! DataRecipe
            }
            
            public static func DataRecipeFromRecipe(recipe: Recipe, storage: NSManagedObjectContext) -> DataRecipe {
                let dataRecipe = createRecipeEntity(storage: storage)
                
                dataRecipe.id = Int32(recipe.id!)
                dataRecipe.image = recipe.image!
                dataRecipe.imageType = recipe.imageType!
                dataRecipe.instructions = recipe.instructions!
                dataRecipe.readyInMinutes = Int16(recipe.readyInMinutes!)
                dataRecipe.servings = Int16(recipe.servings!)
                dataRecipe.title = recipe.title
                dataRecipe.week = Int16(recipe.week!)
                dataRecipe.day = Int16(recipe.day!)
                
                return dataRecipe
            }
            
            public static func One(recipe: Recipe) -> Bool {
                // First delete if the current one exist
                Storage.Recipes.Delete.One(recipe: recipe)
                
                let storage = Storage.GetManagedContext()!
                
                if recipe.extendedIngredients != nil {
                    print("Successfully saved: ", Ingredients.Save.RecipeIngredients(recipe: recipe, week: recipe.week!))
                }
                
                _ = DataRecipeFromRecipe(recipe: recipe, storage: storage)
                
                do {
                    // Then save it. I.e. overwrite
                    try storage.save()
                    return true
                } catch let error as NSError {
                    printError(error: error)
                    return false
                }
            }
            
            public static func Many(recipes: [Recipe]) -> Bool {
                for recipe in recipes {
                    if !One(recipe: recipe) {
                        return false
                    }
                }
                return true
            }
        }
        
        class Delete {
            public static func One(recipe: Recipe) {
                let storage = Storage.GetManagedContext()!
                let fetchRequest = Recipes.GetFetchRequest()
                fetchRequest.predicate = NSPredicate(format: "week = %@ AND day = %@", argumentArray: [recipe.week!, recipe.day!])
                
                if let result = try? storage.fetch(fetchRequest) {
                    for object in result {
                        storage.delete(object)
                    }
                    try? storage.save()
                }
            }
            
            public static func Many(recipes: [Recipe]) {
                for recipe in recipes {
                    Delete.One(recipe: recipe)
                }
            }
        }
    }
    
    class Ingredients { // try Storage.GetManagedContext()!.fetch(Recipes.GetFetchRequest()) as! [DataRecipe]
        public static func createIngredientEntity(storage: NSManagedObjectContext) -> DataIngredient {
            return NSEntityDescription.insertNewObject(forEntityName: "DataIngredient", into: storage) as! DataIngredient
        }
        
        public static func GetFetchRequest() -> NSFetchRequest<NSManagedObject> {
            return NSFetchRequest<NSManagedObject>(entityName: "DataIngredient")
        }
        
        public static func DataIngredientFromIngredient(ingredient: Ingredient, storage: NSManagedObjectContext) -> DataIngredient {
            let dataIngredient = createIngredientEntity(storage: storage)
            
            dataIngredient.id = Int32(ingredient.id ?? -1)
            dataIngredient.amount = NSDecimalNumber(value: ingredient.amount ?? -1)
            dataIngredient.image = ingredient.image
            dataIngredient.name = ingredient.name
            dataIngredient.unit = ingredient.unit
            dataIngredient.unitShort = ingredient.unitShort
            dataIngredient.unitLong = ingredient.unitLong
            dataIngredient.week = Int16(ingredient.week!)
            dataIngredient.recipeID = Int32(ingredient.recipeID!)
            
            return dataIngredient
        }
        
        
        class Get {
            static func printError(error: NSError) {
                print("Could not get groceries \n\(error). \(error.userInfo)")
            }
            
            public static func Many(predicate: NSPredicate) -> [Ingredient]? {
                let fetchRequest = Ingredients.GetFetchRequest()
                fetchRequest.predicate = predicate
                
                do {
                    let storage = Storage.GetManagedContext()!
                    let dataIngredients = try storage.fetch(fetchRequest) as! [DataIngredient]
                    var ingredients = [Ingredient]()
                    
                    for i in dataIngredients {
                        ingredients.append(Ingredient(ingredient: i))
                    }
                    
                    return ingredients
                } catch let error as NSError {
                    printError(error: error)
                    return nil
                }
            }

            public static func Week(week: Int) -> [Ingredient]? {
                return Many(predicate: NSPredicate(format: "week = \(week)"))
            }
            
            public static func RecipeIngredients(recipe: Recipe) -> [Ingredient]? {
                return Many(predicate: NSPredicate(format: "recipeID = \(recipe.id!)"))
            }
        }
        
        class Save {
            static func printError(error: NSError) {
                print("Could not save in ingredients \n\(error). \(error.userInfo)")
            }
            
            public static func One(ingredient: Ingredient) -> Bool {
                let storage = Storage.GetManagedContext()!
                _ = Storage.Ingredients.DataIngredientFromIngredient(ingredient: ingredient, storage: storage)
                
                do {
                    try storage.save()
                    return true
                } catch let error as NSError {
                    printError(error: error)
                    return false
                }
            }
            
            public static func Many(ingredients: [Ingredient]) -> Bool {
                for ingredient in ingredients {
                    if !One(ingredient: ingredient) {
                        return false
                    }
                }
                return true
            }
            
            public static func RecipeIngredients(recipe: Recipe, week: Int) -> Bool {
                if recipe.extendedIngredients != nil {
                    for ingredient in recipe.extendedIngredients! {
                        ingredient.week = week
                        ingredient.recipeID = recipe.id
                        if !One(ingredient: ingredient) { return false }
                    }
                }
                return true
            }
        }
        
        class Delete {
            public static func Week(week: Int) {
                let storage = Storage.GetManagedContext()!
                let fetchRequest = Ingredients.GetFetchRequest()
                fetchRequest.predicate = NSPredicate(format: "week = \(week)")
                
                /* we have a very strange bug here.
                 * It seems to not successfully delete the week yet we get the right ones
                 * As seen from prints it deletes the week but it always deletes more than it should.
                 * We believe that it never deletes anything which is weird since we call delete and then saves the storage.
                 * How and why this is happening we cannot tell.
                 */
                do {
                    let result = try storage.fetch(fetchRequest)
                    for object in result {
                        storage.delete(object)
                        print("deleted")
                    }
                    try storage.save()
                } catch let error as NSError {
                    print("There was an error deleting the current week:(", error.userInfo)
                }
            }
        }
    }
}
